��    w      �  �   �      
  
   
     $
     3
     D
     Q
     U
  '   c
     �
     �
  !   �
  !   �
  #   �
       2   '  	   Z     d     y  "   �     �  	   �     �     �     �          )     7     O     o     �     �     �      �     �                (  	   @  #   J     n     s     �     �     �     �     �     �     �  	   �     �     �     �     �          
             +   (  )   T  '   ~     �     �     �     �  "   �               /     ?     M     \     k  3   |     �     �     �               9  
   N     Y     _     g           �     �  "   �     �     �  '        9  #   ?  "   c      �     �  $   �     �  
   �  
     
          	   +     5  	   A     K     S  	   Z     d     l       
   �     �     �     �  	   �     �     �     �     �  �  �     s     �     �     �     �     �  #   �     �               (     A     Z  3   r     �     �     �  $   �     	            !   /  !   Q     s     �     �     �     �     �               *     I     Y     i     y  	   �  !   �     �     �     �     �        	                  )  
   B     M     T     b     i     m  
   t          �  &   �  "   �  &   �               $     =  !   S  	   u          �     �     �     �     �  0   �       "   3     V     p  #   �     �  
   �     �  	   �     �     �     �  
     !   %     G      Y     z     �  !   �  !   �     �               1     Q     X     _     l     {     �     �  
   �  
   �  
   �  
   �     �  
   �  
   �     �       
        %     3  
   H     S  x   a         H   n   )   3      .       w   2   @           [   Q   B                  k   \   F   +       =   6   i   v      o                  0   s   q   #   g   f   9   ?          G   b   j   1   <       S   P       	             l      Y       D   E   C      U   L                 T   /   (              _             A                                     J           m   d       u   -       `         a   X           :   '       
      I   %      ^   Z      N      *   t               M   r   5   !   4   h                  R           >   e   c           V   O       8      $   ,       K   ;   W   &   p   7   ]       "    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Are you sure you want to log out? Are you sure you want to restart? Are you sure you want to shut down? Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Do you want to suspend to RAM? Do you want to suspend to disk? Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to hibernate. Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to restart. Failed to shut down. Failed to suspend. Failed to switch users. Favorites Hibernating computer in %d seconds. Icon Icon and title Include _favorites Ite_m icon size: Large Larger Log Ou_t... Log _Out Logging out in %d seconds. Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Restarting computer in %d seconds. Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Session Commands Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show c_onfirmation dialog Show cate_gory names Show generic application _names Show menu hie_rarchy Shut _Down Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Stay _visible when focus is lost Suspe_nd Suspending computer in %d seconds. Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Turning off computer in %d seconds. Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Hibernate _Icon: _Lock Screen _OK _Pattern: _Regular expression _Restart _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-04-02 06:50+0000
Last-Translator: 玉堂白鹤 <yjwork@qq.com>
Language-Team: Chinese (China) (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/zh_CN/)
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 添加动作 添加到桌面 添加到收藏夹 添加到面板 所有 所有设置 (_S) Xfce 应用程序启动器替代品 项目合计(_I): 应用程序 您确定要注销吗？ 您确定要重启吗？ 您确定要关机吗？ 背景不透明度(_Y): 浏览文件系统以选择一个自定义命令。 命令 (_O):  类目图标大小 (_O):  清除最近使用项 版权所有© 2013-2019 Graeme Gott 细节 显示 (_S):  默认显示(_D) 您确定要挂起到内存吗？ 您确定要挂起到硬盘吗？ 编辑应用程序... 编辑个人资料(_P) 编辑个人资料失败 执行命令 "%s" 失败。 休眠失败。 执行菜单编辑器失败。 锁定屏幕失败。 注销失败。 打开设置管理器失败。 重启失败。 关机失败。 挂起失败。 切换用户失败。 收藏夹 计算机将在%d 秒内休眠。 图标 图标和标题 包含收藏夹(_F) 项目图标大小 (_M):  大 非常大 注销 (_T)... 注销 (_O) 将在%d 秒内注销。 Man 页面 菜单 名称 (_E):  名称 无 正常 打开 URI 面板按钮 模式 在面板按钮旁放置搜索框 (_S) 在面板按钮旁放置类目(_G) 在搜索框旁放置命令列表 (_E) 最近使用 从收藏夹移除 移除动作 "%s" 吗？ 移除选中的动作 计算机将在%d 秒内重启。 运行 %s 在终端模拟器运行 搜索动作 (_N) 搜索动作 选择一个图标 选择命令 会话命令 显示一个易于访问已安装程序的菜单 显示应用程序描述 (_D) 显示应用程序工具提示(_L) 显示确认对话框(_C) 显示类别名称(_G) 显示通用应用程序名称 (_N) 显示层级菜单 (_R) 关机(_D) 小 非常小 以 A-Z 排列 以 Z-A 排列 焦点丢失时保持可见(_V) 挂起(_N) 计算机将在%d 秒内挂起。 切换用户 (_U) 悬浮鼠标以切换类目 (_H) 此动作将被永久删除。 标题 计算机将在%d 秒内关闭。 无法添加启动器到桌面。 无法添加启动器到面板/ 无法编辑启动器。 无法打开url: %s 只占用面板中的一行(_P) 极大 极小 网页搜索 Whisker 菜单 维基百科 外观 (_A) 行为 (_B) 取消(_C) 关闭(_C) 命令(_C) 删除(_D) 编辑应用程序 (_E) 帮助(_H) 休眠(_H) 图标 (_I):  锁定屏幕 (_L) 确定(_O) 模式 (_P):  正则表达式 (_R) 重启(_R) 标题 (_T):  白铭骢 <jeffbaichina@members.fsf.org>, 2014\n
Tao Lin <lintao51@gmail.com>, 2015\n
玉堂白鹤 <yjwork@qq.com>, 2015 