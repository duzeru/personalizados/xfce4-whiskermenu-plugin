��    a      $  �   ,      8  
   9     D     S     d     q     u  '   �     �     �     �  2   �  	   	     	     1	     E	  	   M	     W	     k	     	     �	     �	     �	     �	     �	      
     .
  	   F
     P
     U
     d
     w
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
  +   �
  )     '   9     a     o     �     �     �     �     �     �     �     �  3        7     V     q     �     �     �     �     �     �      �          (  '   G     o  "   u      �     �  $   �     �  
     
     
   %     0  	   =     G  	   S     ]     e  	   l     v     ~     �     �     �     �  	   �     �     �     �  �  �  +   o  7   �  8   �  2        ?  "   O  r   r  )   �  $     D   4  �   y       3   &  e   Z     �     �  B   �  Z   6  V   �  T   �  Z   =  L   �  ;   �  &   !  ^   H  A   �     �        *     D   >  6   �     �     �  5   �  (        :  
   M  	   X      b     �  1   �     �     �  �   �  �   }  z     6   �  ?   �  ?     H   E  +   �  D   �  7   �  .   7  9   f  3   �  �   �  T   {  S   �  G   $  i   l  R   �     )     6  S   K  R   �  `   �  @   S   W   �   J   �      7!  c   K!  ]   �!  ]   "  M   k"  a   �"  "   #  #   >#     b#  %   v#     �#  	   �#     �#     �#     �#     �#  !   $  V   0$     �$     �$  8   �$  	   �$     �$  1   �$     1%  #   E%         [   !          8   4   9   <           J   Z   ,   a      W      A       -   &   5   =         `          +   Y   B   :       Q   %   _   L          7             \       M                           
   S   G       6      ^       '          *   $       I       	               @   T      3                         )   ?          N      0       V   H      R      >       1   "   X   /   F      O   ;                        ]   D       2   P   U   E          K   .      (   #           C    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Include _favorites Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Stay _visible when focus is lost Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-18 08:12+0000
Last-Translator: starryprabin <mr.prabin@gmail.com>
Language-Team: Nepali (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/ne/)
Language: ne
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 कार्य थप्नुहोस् डेस्कटपमा थप्नुहोस् मन पर्नेमा थप्नुहोस् पट्टीमा थप्नुहोस्  समस्त समस्त सेटिङ् Xfce को लागि वैकल्पिक अनुप्रयोग सुरुवातकर्ता वस्तुको परिमाण: अनुप्रयोगहरु पृष्ठभूमिको अपारदर्शिता: एक व्यक्तिगत आदेशको चयन गर्न फाइल प्रणाली ब्राउज गर्नुहोस् आदेश: वर्गमूर्तिको आकार:  हालसालै प्रयोग भएकालाई सफा गर्नुहोस्  विवरणहरु प्रदर्शन: स्व:त प्रदर्शन गर्नुहोस् अनुप्रयोगलाई सम्पादन गर्नुहोस् ... पाश्र्वचित्र सम्पादन गर्नुहोस् पाश्र्वचित्र सम्पादन गर्न बिफल "%s" आदेशलाई कार्यान्वयन गर्न  बिफल। मेनु सम्पादक सुरु गर्न बिफल। स्क्रिन लक गर्न  बिफल । बाहिरीन बिफल । प्राथमिकता व्यवस्थापक खोल्न  बिफल। प्रयोगकर्ता फेर्न बिफल। मन पर्ने मूर्ति मूर्ति र शीर्षक  मनपर्ने समावेश गर्नुहोस् वस्तुमूर्तिको आकार:  ठूलो अझ ठूलो बाहिर _निस्किनुहोस् सहयोग पृष्ठहरु मेन्यू नाम: नाम केहि पनि छैन साधारण युआरआइ खोल्नुहोस् पट्टीको बटन ढाँचा पट्टीको बटनको नजिक खोजलाई स्थानाङ्कित गर्नुहोस्  पट्टीको बटनको नजिक श्रेणीहरुलाई स्थानाङ्कित गर्नुहोस् खोजको नजिक आदेशहरुलाई स्थानाङ्कित गर्नुहोस्  हालसालै प्रयोग भएका  मन पर्नेबाट हटाउनुहोस्  के "%s" कार्य हटाउनु हुन्छ? चयन गरेको कार्य हटाउनुहोस् %s चालु गर्नुहोस् टर्मिनलमा चालु गर्नुहोस् कार्यहरु खोज्नुहोस् कार्य खोज्नुहोस् मूर्ति चयन गर्नुहोस्  आदेश चयन गर्नुहोस्  स्थापित अनुप्रयोगहरुको सहज पहुचको लागी एक मेन्यू देखाउनुहोस् अनुप्रयोगका विवरण देखाउनुहोस्  अनुप्रयोगका सुझाव देखाउनुहोस् वर्गका नामहरू देखाउनुहोस् अनुप्रयोगहरुको समान्य नाम देखाउनुहोस् मेन्यू  पदक्रमलाई देखाउनुहोस्  सानो अझ सानो  A-Z  अनुसार वर्णक्रमीक गर्नुहोस्  Z-A अनुसार वर्णक्रमीक गर्नुहोस्   ध्यान हराउँदा दृश्यात्मक रहनुहोस्  प्रयोगकर्ता फेर्नुहोस् मण्डराएर वर्गहरुलाई बदल्नुहोस्  कार्य स्थायीरुपमा हटाईनेछ। शीर्षक  प्रक्षेपकलाई डेस्कटपमा थप्न असमर्थ। प्रक्षेपकलाई पट्टीमा थप्न असमर्थ। प्रक्षेपकलाई सम्पादन गर्न असमर्थ। निम्न युआरएललाई खोल्न बिफल: %s  एक पङ्क्ति पट्टीको प्रयोग गर्नुहोस् अत्यन्त ठूलो अत्यन्त सानो  वेब खोज विस्कर मेन्यू विकिपीडिया रुप ब्य॒वहार रद्द बन्द आदेशहरू मेटाउनुहोस् अनुप्रयोगहरु सम्पादन गर्नुहोस् मद्दत मूर्ति: स्क्रिन लक गर्नुहोस् ठीक ढाँचा: रेगुलर एक्सप्रेशन शीर्षक: अनुवादक-आभार  