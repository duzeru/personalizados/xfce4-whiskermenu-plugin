��    a      $  �   ,      8  
   9     D     S     d     q     u  '   �     �     �     �  2   �  	   	     	     1	  "   E	     h	  	   p	     z	     �	     �	     �	     �	     �	     
     
      0
     Q
  	   i
     s
     x
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
  +     )   2  '   \     �     �     �     �     �     �     �     �            3   &     Z     y     �     �     �     �     �     �               *  '   I     q  "   w      �     �  $   �     �  
     
     
   '     2  	   ?     I  	   U     _     g  	   n     x     �     �     �     �     �  	   �     �     �     �  �  �     �     �     �     �     �     �  *        /  
   C     N  E   d  	   �     �     �  "   �       	        #     B     Z     n  !   �  $   �     �     �  .        >     ]  	   f     p     �      �     �     �     �  
   �     �     �     �     �     �     �            =     4   W  4   �     �     �     �          $     0     F     W     g       :   �  !   �  (   �       #   6     Z     t     x     �     �     �  '   �  +   �       )   !  '   K     s  '   �  $   �     �  
   �     �       	             $  
   2  	   =     G     P     Y     o     w     �     �     �     �     �     �         [   "          9   5   :   =           J   Z   -   a      W      B       .   '   6   >         `         ,   Y   C   ;       Q   &   _   L          8             \       M                           
   S   G       7       ^       (          +   %       I       	               A   T      4                         *   @          N      1       V   H      R      ?       2   #       0   X      O   <                        ]   F       3   P   U   E          K   /      )   $   !       D    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Include _favorites Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-04-20 21:11+0000
Last-Translator: Sebastian Brici <bricisebastian@gmail.com>
Language-Team: Romanian (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/ro/)
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
 Adaugă acțiune Adaugă la desktop Adaugă la favorite Adaugă la panou Toate Toate _setările Lansator de aplicații alternativ pt. Xfce Număr de _elemente Aplicații Opac_itatea fondului: Caută sistemul de fișiere pentru a găsi o comandă personalizată. C_omandă Mărimea iconiței categ_oriei: Șterge recent folositele Copyright © 2013-2019 Graeme Gott Detalii Afi_șaj: Arată după stare i_mplicită Editează aplicația... Editează _profilul A eșuat editarea profilului. A eșuat executarea comenzii "%s" Nu se poate lansa editorul de meniu. A eșuat blocarea ecranului. A eșuat închiderea sesiunii. A eșuat deschiderea gestionarului de setări. Nu se pot schimba utilizatorii Favorite Iconiță Iconiță și titlu Include _favorite _Mărimea iconiței articolului: Mare Mai mare _Ieșire Pagini Man Meniu Num_e Nume Niciunul Normal Deschide URL Buton panou Tipar Poziționează căsuța de căutare _lângă butonul de panou Poziționează cate_goriile lângă butonul de panou Poziționează comen_zi lângă căsuța de căutare Folosite recent Șterge din favorite Reliminați acțiunea „%s”? Elimină acțiunea selectată Rulează %s Rulează în terminal Caută acți_uni Caută acțiune Selectează o iconiță Alegeți comanda Arată un meniu pentru a accesa ușor aplicații instalate Arată _descrierile aplicațiilor Arată _recomandările pentru aplicații Arată _denumirile categoriilor Arată _nume generice de aplicații Arată ie_rarhia meniului Mic Mai mic Sortează alfabetic A-Z Sortează alfabetic Z-A Schimbă _utilizatorii Schimbă categoriile _planând cursorul Această acțiune va fi ștearsă permanent Titlu Nu s-a putut adăuga lansator la desktop. Nu s-a putut adăuga lansator la panou. Eșec la editarea lansatorului. Nu se poate deschide următorul url: %s Folosește un singur rând de _panou Foarte mare Foarte mic Căutare web Meniu Whisker Wikipedia _Aspect Compo_rtament _Anulează Înch_ide _Comenzi Șt_erge _Editează aplicații _Ajutor _Iconiță: B_lochează ecranul _OK Ti_par: Expresie _regulată _Titlu autori-traduceri 