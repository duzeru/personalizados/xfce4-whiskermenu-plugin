��    ^           �      �  
   �               $     1     5  '   C     k     }     �  2   �  	   �     �     �  "   	     (	  	   0	     :	     N	     \	     t	     �	     �	     �	      �	     �	  	   
     
     $
     3
     D
     J
     Q
  	   Z
     d
     i
     p
     u
     z
     �
     �
     �
  +   �
  )   �
  '   �
          +     A     U     l     s     �     �     �     �  3   �     �          -     B     b     w     }     �     �     �     �  '   �     
  "         3  $   T     y  
   �  
   �  
   �     �  	   �     �  	   �     �     �  	   �     �                           -  	   1     ;     O     W  �  j     �  $     %   6     \     v      �  7   �     �     �  !     m   3     �  ,   �  ?   �  "        B     S  8   d      �  >   �  G   �  N   E  >   �  6   �  Q   
  J   \     �     �  "   �  0   �  
        (  	   ;     E  
   \     g     p     w     ~  
   �     �     �  d   �  a   &  [   �  !   �  )     1   0  .   b     �  #   �      �     �  #        &  x   F  C   �  N     /   R  J   �  1   �     �       '     '   ?  ,   g  Q   �  H   �  
   /  R   :  G   �  @   �  .        E     W     q     �     �     �     �     �  	   �     �       (        E     Q     b     �  
   �     �     �  /   �     G      ,   "   :   0   '   ?          R   9      H               Z   M              X   D   *      +       S       N   -   1   ;   P      =      ^   (   !   @             Q   8       Y           F   4   C   L   W   E      >                %   6          ]       I   V       B       .                                    K   5   \       2   &   [   /           	         $       7   3                        U      O   <      J      
             T   A       #              )    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-12-02 11:51+0000
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
Language-Team: Kazakh (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/kk/)
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Әрекетті қосу Жұмыс үстеліне қосу Таңдамалыларға қосу Панельге қосу Барлығы Барлық ба_птаулар Xfce үшін қолданбалар жөнелтуші Нәрселер _саны: Қолданбалар Фон мөл_дірлілігі: Таңдауыңызша команданы таңдау үшін файлдық жүйені шолыңыз. К_оманда: Сан_ат таңбашасы өлшемі: Соңғы қолданылған тізімін тазалау Copyright © 2013-2019 Graeme Gott Ақпараты Көр_сету: Үн_сіз келісім бойынша көрсету _Профильді түзету Профильді түзету сәтсіз аяқталды. "%s" командасын орындау сәтсіз аяқталды. Мәзір түзетушісін жөнелту сәтсіз аяқталды Экранды бұғаттау сәтсіз аяқталды. Жүйеден шығу сәтсіз аяқталды. Баптаулар басқарушысын ашу сәтсіз аяқталды. Пайдаланушыны ауыстыру сәтсіз аяқталды. Таңдамалы Таңбаша Таңбаша және атауы Эле_мент таңбашасы өлшемі: Үлкен Үлкенірек Ш_ығу Man парақтары Мәзір А_ты: Аты Жоқ Қалыпты URI ашу Панель батырмасы Үлгі Іздеу өрі_сін панель батырмасының қасында орналастыру Са_наттарды панель батырмасының қасында орналастыру Командаларды іздеу өрі_сінің қасында орналастыру Соңғы қолданылған Таңдамалылардан өшіру "%s" әрекетін өшіру керек пе? Таңдалған әрекетті өшіру %s орындау Терминалда орындау Іздеу әре_кеттері Іздеу әрекеті Таңбашаны таңдаңыз Команданы таңдау Орнатылған қолданбаларға қатынау үшін қарапайым мәзірді көрсету Қолданбалар с_ипаттамаларын көрсету Қолданбалар қ_алқымалы кеңестерін көрсету Сан_аттар аттарын көрсету Қолданбалардың жалпы ата_уларын көрсету Мәзір и_ерархиясын көрсету Кіші Кішірек Әліппемен сұрыптау A-Z Әліппемен сұрыптау Z-A Пайдаланушыны _ауыстыру Санаттарды үстіне а_пару арқылы ауыстырыңыз Бұл әрекет қайтарылмайтындай жойылады. Атауы Жөнелткішті жұмыс үстеліне қосу мүмкін емес. Жөнелткішті панельге қосу мүмкін емес. Келесі сілтемені ашу мүмкін емес: %s Бір _панель жолын қолдану Өте үлкен Өте кішкентай Веб іздеуі Whisker мәзірі Википедия С_ыртқы түрі Мі_нез-құлығы Ба_с тарту _Жабу _Командалар Ө_шіру Қолданбаларды түз_ету _Көмек _Таңбаша: Экранды б_локтау _ОК Үл_гі: Тұ_рақты өрнек А_тауы: Baurzhan Muftakhidinov <baurthefirst@gmail.com> 