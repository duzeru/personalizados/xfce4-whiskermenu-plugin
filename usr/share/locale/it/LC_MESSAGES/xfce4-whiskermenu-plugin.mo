��    `        �         (  
   )     4     C     T     a     e  '   s     �     �     �  2   �  	   	     	     !	  "   5	     X	  	   `	     j	     ~	     �	     �	     �	     �	     �	     
       
     A
  	   Y
     c
     h
     w
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
  +   �
  )     '   9     a     o     �     �     �     �     �     �     �     �  3        7     V     q     �     �     �     �     �     �     �       '   &     N  "   T      w     �  $   �     �  
   �  
   �  
          	        &  	   2     <     D  	   K     U     ]     p     v     }     �  	   �     �     �     �  �  �     X     k          �     �     �  2   �     �            ?   4  	   t     ~     �  "   �     �  	   �     �               +  %   K  %   q      �     �  1   �       	   $     .     4     C     [     b     n  	   w     �     �     �     �     �     �     �     �  <   �  8     ,   =     j     {     �     �  	   �     �     �     �     �       A   &  *   h     �     �  *   �     �     	               9     T  (   i  -   �     �  -   �  .   �  "   $  %   G  )   m     �     �     �     �  	   �     �     �  	   �     �       	             *     1     9  	   I     S     \     r     {         Z   !          8   4   9   <           I   Y   ,   `      V      A       -   &   5   =         _         +   X   B   :       P   %   ^   K          7             [       L                           
   R   F       6       ]       '          *   $       H       	               @   S      3                         )   ?          M      0       U   G      Q      >       1   "       /   W      N   ;                        \   E       2   O   T   D          J   .      (   #           C    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-04-12 23:53+0000
Last-Translator: Patrizio Bellan <patrizio.bellan@gmail.com>
Language-Team: Italian (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/it/)
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Aggiungi un'azione Aggiungi al Desktop Aggiungi ai Preferiti Aggiungi al Pannello Tutto Tutte le _Impostazioni Lanciatore delle applicazioni alternativo per Xfce Quantità di _items: Applicazioni Opacit_à dello sfondo: Sfoglia il file system per scegliere un comando personalizzato. C_omando: Dimensione icona Categ_oria: Cancella Usati di recente Copyright © 2013-2019 Graeme Gott Dettagli Di_splay: Visualizza per _default Modifica Applicazione... Edita _Profilo Impossibile editare il profilo. Impossibile eseguire il comando "%s". Impossibile avviare l'editor di menu. Impossibile bloccare lo schermo. Disconnessione non riuscita. Impossibile aprire il Gestore delle impostazioni. Impossibile cambiare utente. Preferiti Icona Icona e titolo Dimensione icona Ite_m: Grande Più grande Log _Out Man Pages Menu Nom_e: Nome Niente Normale Apri URI Pulsante del pannello Motivo Posizionare il campo _Cerca accanto al pulsante del pannello Posizionare cate_gorìe accanto al pulsante del pannello Posizionare comandi  accanto al _campo cerca Usati di recente Rimuovi dai Preferiti Rimuovi azione "%s"? Rimuovi azione selezionata Esegui %s Esegui nel terminale Ricerca azio_ni Ricerca azioni Selezionare un'icona Seleziona comando Mostra un menu per accedere facilmente le applicazioni installate mostra una _descrizione delle applicazioni Mostra too_ltips Mostra i nomi delle cate_gorie Mostra i _nomi generici delle applicazioni Mostra ge_rarchia dei menu Piccolo Più piccolo Ordina alfabeticamente A-Z Ordina alfabeticamente Z-A Commutazione _utenti Cambia categorie al _passaggio del mouse L'azione verrà eliminata in modo permanente. Titolo Impossibile aggiungere lanciatore al desktop. Impossibile aggiungere lanciatore al pannello. Impossibile modificare il launcher Impossibile aprire il collegamento %s Utilizzare una singola riga nell _panello Molto grande Molto piccolo Ricerca nel web Menu Whisker Wikipedia _Aspetto _Comportamento _Cancella _Chiudi _Comandi _Cancella _Edita applicazioni _Aiuto _Icona: _Blocca schermo _Conferma _Motivo: _Espressione regolare _Titolo: translator-credits 