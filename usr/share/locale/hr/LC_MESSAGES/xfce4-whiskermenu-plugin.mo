��    a      $  �   ,      8  
   9     D     S     d     q     u  '   �     �     �     �  2   �  	   	     	     1	  "   E	     h	  	   p	     z	     �	     �	     �	     �	     �	     
     
      0
     Q
  	   i
     s
     x
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
  +     )   2  '   \     �     �     �     �     �     �     �     �            3   &     Z     y     �     �     �     �     �     �               *  '   I     q  "   w      �     �  $   �     �  
     
     
   '     2  	   ?     I  	   U     _     g  	   n     x     �     �     �     �     �  	   �     �     �     �  �  �     �     �     �     �     �       )        ;  
   H     S  .   g  	   �     �  )   �  '   �       
        "     9     J     W  $   t  ,   �  $   �     �  +      #   ,     P     Y     _     k     }     �     �     �     �     �     �     �     �     �  
   �     �     �  .   �  )   +  $   U     z     �     �     �  
   �     �     �               &  C   8     |  +   �     �  %   �               #     )     G     e  %   x  !   �     �  1   �  '   �       (   ;     d     �  	   �     �     �  	   �     �     �  	   �     �     �  	   �                     !     4     <     E     U     ^         [   "          9   5   :   =           J   Z   -   a      W      B       .   '   6   >         `         ,   Y   C   ;       Q   &   _   L          8             \       M                           
   S   G       7       ^       (          +   %       I       	               A   T      4                         *   @          N      1       V   H      R      ?       2   #       0   X      O   <                        ]   F       3   P   U   E          K   /      )   $   !       D    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Include _favorites Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-05-19 08:37+0000
Last-Translator: dmanthing <dmanthing@gmail.com>
Language-Team: Croatian (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/hr/)
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 Dodaj akciju Dodaj na Radnu površinu Dodaj u Omiljeno Dodaj na ploču Sve Sve _Postavke Alternativni pokretač aplikacija za Xfce Broj stavki: Aplikacije Prozirnost pozadine Pretraži datotečni sustav za odabir naredbe. N_aredba: Veličin_a ikone kategorije: Očisti kategoriju Najčešće korišteno Autorska prava © 2013-2019 Graeme Gott Detalji Pri_kaži: Prema zadanom prikaži Uredi aplikaciju Uredi profil Nije moguće urediti profil. Nije moguće izvršiti naredbu "%s". Nije moguće pokrenuti uređivač izbornika. Zaključavanje zaslona nije moguće. Odjava nije moguća. Nije moguće otvoriti Upravitelja Postavki. Nije moguće promijeniti korisnika. Omiljeno Ikona Ikona i ime Uključi omiljene Veličina ik_one stavke: Veliko Veće O_djava Man Stranice Izbornik I_me: Ime Ništa Normalno Otvori URL Gumb na ploči Uzorak Postavi _pretraživanje kraj dugmeta na ploči Postavi kategorije pokraj gumba na ploči Postavi komande kraj pret_raživanja Najčešće korišteno Obriši iz grupe Omiljeno Ukloni akciju "%s"? Ukloni odabranu akciju Pokreni %s Pokreni u Terminalu Pretraži a_kcije Pretraži akcije Izaberi ikonu Odaberite Komandu Prikaži izbornik da bi lakše pristupili instaliranim aplikacijama Prikazi _opis aplikacije Prikazati savjete za korištenje aplikacije Prikaži nazive kategorija Prikaži generička i_mena aplikacija Prikaži poredak izbo_rnika Malo Manje Posloži prema abecedi (A-Ž) Posloži prema abecedi (Ž-A) Zamjeni _Korisnike Promjeni kategoriju prij_elazom miša Akcija će biti trajno uklonjena. Ime Nije moguće dodati pokretač na Radnu površinu. Nije moguće dodati pokretač na Ploču Nije moće urediti pokretač Nije moguće otvoriti slijedeći URL: %s Koristi samo jedan redak ploče Jako veliko Jako malo Internet pretraga Whisker Izbornik Wikipedia Izg_led Pon_ašanje _Odustani _Zatvori Komande _Izbriši _Uredi Aplikacije _Pomoć _Ikona _Zaključaj Zaslon _U redu _Uzorak: _Normalan izraz _Naslov: Zasluge za prijevod 