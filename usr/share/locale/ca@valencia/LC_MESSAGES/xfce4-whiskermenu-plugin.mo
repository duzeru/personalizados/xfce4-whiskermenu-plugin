��    `        �         (  
   )     4     C     T     a     e  '   s     �     �     �  2   �  	   	     	     !	  "   5	     X	  	   `	     j	     ~	     �	     �	     �	     �	     �	     
       
     A
  	   Y
     c
     h
     w
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
  +   �
  )     '   9     a     o     �     �     �     �     �     �     �     �  3        7     V     q     �     �     �     �     �     �     �       '   &     N  "   T      w     �  $   �     �  
   �  
   �  
          	        &  	   2     <     D  	   K     U     ]     p     v     }     �  	   �     �     �     �  �  �     {     �     �     �     �     �  4   �          +     7  E   K     �  &   �     �  "   �     �               '     =     N  (   n  .   �  %   �  %   �  1        D  	   a     k     q  "   �     �  	   �     �     �     �     �     �     �     �     �     �       8     4   P  /   �     �     �  !   �  !        )     8     L     _     p     �  H   �  )   �  4         D  -   e     �     �     �     �     �       .     '   E     m  7   t  2   �  "   �  "     "   %  	   H     R     `     m     {  
   �     �     �     �     �  
   �     �     �     �     �  
                  *  �   3         Z   !          8   4   9   <           I   Y   ,   `      V      A       -   &   5   =         _         +   X   B   :       P   %   ^   K          7             [       L                           
   R   F       6       ]       '          *   $       H       	               @   S      3                         )   ?          M      0       U   G      Q      >       1   "       /   W      N   ;                        \   E       2   O   T   D          J   .      (   #           C    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 02:38+0000
Last-Translator: Jose Alfredo Murcia Andrés <joamuran@gmail.com>
Language-Team: Catalan (Valencian) (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/ca@valencia/)
Language: ca@valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Afig una acció Afig a l’escriptori Afig als preferits Afig al quadre Totes Tots els paràmetres_s Accés directe a aplicacions alternatiu per a l'Xfce Nombre d’e_lements: Aplicacions Opacitat del _fons: Navegueu pel sistema de fitxers per a triar una ordre personalitzada. _Ordre: Mida de les icones de les categ_ories: Neteja els elements recents Copyright © 2013-2019 Graeme Gott Detalls Mo_stra: Mostra-ho per _defecte Edita l'aplicació... Edita el _perfil Ha fallat l'edició del perfil. Ha fallat l'execució de l'ordre «%s». Ha fallat l'execució de l'editor dels menús. Ha fallat el bloqueig de la pantalla. Ha fallat el tancament de la sessió. Ha fallat l'obertura del gestor de configuració. Ha fallat el canvi d'usuari. Preferits Icona Icona i títol Mida de les icones dels ele_ments: Gran Més gran Ta_nca la sessió Pàgines del manual Menú N_om: Nom Cap Normal Obri un URI Botó del quadre Patró _Situa el quadre de cerca al costat del botó del quadre Situa les cate_gories al costat del botó del quadre S_itua les ordres al costat del quadre de cerca Utilitzades recentment Suprimeix dels preferits Voleu suprimir l’acció «%s»? Suprimeix l’acció seleccionada Executa «%s» Executa al terminal Cerca d’accio_ns Cerca l’acció Seleccioneu una icona Seleccioneu una ordre Mostra un menú per a accedir fàcilment a les aplicacions instal·lades Mostra la _descripció de les aplicacions Mostra els _indicadors de funció de les aplicacions Mostra el nom de les cate_gories Mostra els _noms genèrics de les aplicacions Mostra la je_rarquia del menú Xicoteta Més xicoteta Ordena alfabèticament A–Z Ordena alfabèticament Z–A Canvia d’_usuari Canvia de categoria en a_puntar amb el ratolí Se suprimirà l’acció permanentment. Títol No s’ha pogut afegir a l'escriptori l'accés directe. No s’ha pogut afegir al quadre l'accés directe. No s'ha pogut editar el carregador No es pot obrir l'URL següent: %s Utilitza una sola fila del _quadre Molt gran Molt xicoteta Busca al Web Menú Whisker Viquipèdia _Aparença _Comportament _Cancel·la _Tanca _Ordres _Suprimeix _Edita les aplicacions _Ajuda _Icona: _Bloqueja la pantalla _D’acord _Patró: Expressió _regular _Títol: Adolfo Jayme Barrientos <fitojb@ubuntu.com>, 2013–2016
Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2016
Oriol Fernandez, 2013 