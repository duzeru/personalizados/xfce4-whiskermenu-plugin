��    `        �         (  
   )     4     C     T     a     e  '   s     �     �     �  2   �  	   	     	     !	  "   5	     X	  	   `	     j	     ~	     �	     �	     �	     �	     �	     
       
     A
  	   Y
     c
     h
     w
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
  +   �
  )     '   9     a     o     �     �     �     �     �     �     �     �  3        7     V     q     �     �     �     �     �     �     �       '   &     N  "   T      w     �  $   �     �  
   �  
   �  
          	        &  	   2     <     D  	   K     U     ]     p     v     }     �  	   �     �     �     �  �  �     �     �     �     �     �     �  (        +  
   <     G  ;   ^     �     �     �  '   �                     *     =     L  )   e  .   �      �     �  *   �  "   %     H     Q     Y     j     �     �     �     �     �     �     �     �     �     �     �     �  :   �  '   *  8   R     �     �  !   �     �  
   �     �               -     ?  7   Q     �     �     �  #   �     �               $     ;     R      e  !   �     �  /   �  )   �  !   	  (   +     T     n  	   z     �     �  
   �     �     �  	   �     �     �     �     �     �  	   �            	   !     +     9     B         Z   !          8   4   9   <           I   Y   ,   `      V      A       -   &   5   =         _         +   X   B   :       P   %   ^   K          7             [       L                           
   R   F       6       ]       '          *   $       H       	               @   S      3                         )   ?          M      0       U   G      Q      >       1   "       /   W      N   ;                        \   E       2   O   T   D          J   .      (   #           C    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-02-16 08:48+0000
Last-Translator: srdjan srdjan <acrvena@gmail.com>
Language-Team: Serbian (Latin) (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/sr@latin/)
Language: sr@latin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Dodaj radnju Dodaj na radnu površ Dodaj u omiljene Dodaj na policu Sve Sve _postavke alternativni pokretac aplikacije za Xfce Kolicina stvari: Aplikacije Pozadinska prozirnost: Razgledaj sistem datoteka radi odabira proizvoljne naredbe. Naredba Veličina ikonica _vrsta: Očisti nedavno korišćeno Autorska prava © 2013-2019 Graeme Gott Pojedinosti Prikaz: Podrazumjevani prikaz Urediti aplikaciju Urediti profil Neuspjeh izmjene profila Nisam uspeo da izvršim naredbu „%s“. Nisam uspeo da pokrenem uređivača izbornika. Nisam uspeo da zaključam ekran. Nisam uspeo da se odjavim. Nisam uspeo da otvorim upravnika postavki. Nisam uspeo da promenim korisnika. Omiljeno Ikonica Ikonica i naslov Veličina ikonice stav_ke: Velike Veće Odja_va Uputne stranice Izbornik I_me: Ime Ništa Obične Otvori adresu Dugme police Obrazac Položaj kućice za unos pretrage u odnosu na dugme police Polozaj kate_gorija do izbornika police Položaj kućice za naredbe u odnosu na kućicu pretrage Nedavno korišćeno Ukloni iz omiljenih Da li da uklonim radnju „%s“? Ukloni označenu radnju Pokreni %s Pokreni u terminalu R_adnje pretrage Radnje pretrage Izaberite ikonicu Izaberite naredbu Prikazuj izbornik za brzi pristup ugrađenim programima Prikazuj _opise programa Pokazi aplikacijske napomene Pokazi imena kategorija Prikazuj uopštene _nazive programa Prikazuj grananje izbornika Male Malecne Poređaj abecedno A-Š Poređaj abecedno Š-A Promeni _korisnika Menjaj vrste _nadnošenjem miša Radnja će biti trajno izbrisana. Naslov Nisam uspeo da dodam pokretač na radnu površ. Nisam uspeo da dodam pokretač na policu. Nisam uspjeo da izmjenim pokretac Nisam uspjeo da otvorim sljedeci url: %s Koristi jedan red police  Vrlo velike Vrlo male Mrezna pretraga Brkin izbornik Vikipedija _Izgled _Ponašanje _Obustavi _Zatvori _Komande _Obrisi _Uredi programe _Pomoc _Ikonica: _Zaključaj ekran _U redu _Obrazac: Opšti iz_raz _Naslov: zasluge -prevodioca 