��    [      �     �      �  
   �     �     �     �            '        ;     M     Z  2   o  	   �     �     �  "   �     �  	    	     
	     	     ,	     D	     d	     �	     �	      �	     �	  	   �	     �	     �	     
     
     
     !
  	   *
     4
     9
     @
     E
     J
     Q
     Z
     g
  +   o
  )   �
  '   �
     �
     �
          %     <     C     S     c     q     �  3   �     �     �                    %     =     U     c  '   �     �  "   �      �  $   �       
   1  
   <     G  	   T     ^  	   j     t     |  	   �     �     �     �     �     �     �  	   �     �     �     �  �  �     �  %   �  7   �  !        #     *  6   F     }     �     �  ]   �     *  '   7  9   _  *   �     �     �  (   �       0   $  -   U  A   �  F   �  '     I   4  4   ~     �  
   �     �  '   �          %  	   5     ?     ^     e     n     u     |     �     �     �  M   �  B   	  G   L  %   �  3   �     �  !        %  $   3      X     y     �     �  `   �  '   /  ;   W  *   �     �     �  >   �  >        U  <   l  1   �  
   �  E   �  ?   ,  5   l  )   �     �     �     �       	             +  	   9     C     S  !   [     }     �  (   �     �  
   �     �     �     �     E      ,   "   :   0   '   =          O   9      F               W                  U   B   *      +       P       K   -   1       M      ;      [   (   !   >             N   8       V           D   4   A   J   T   C      <                %   6          Z       G   S       @       .                                    I   5   Y       2   &   X   /           	         $       7   3                        R      L          H      
             Q   ?       #              )    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to open the following url: %s Use a single _panel row Very Large Very Small Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-04 14:31+0000
Last-Translator: Reza Gharibi <reza.gharibi.rg@gmail.com>
Language-Team: Persian (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/fa/)
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 اضافه‌کردن عمل اضافه کردن به میزکار اضافه‌کردن به علاقه‌مندی‌ها اضافه‌کردن به پنل همه همه‌ی _تنظیمات اجراکننده‌ای جایگزین برای Xfce تعداد _آیتم‌ها برنامه‌ها شفافیت پس‌زمینه مرور سیستم‌فایل برای انتخاب‌کردن یک دستور سفارشی. د_ستور: اندازه‌ی آیکان دست_ه: پاک‌کردن به‌تازگی استفاده شده کپی‌رایت © 2013-2019 Graeme Gott جزئیات نم_ایش: نمایش براساس _پیش‌فرض ویرایش _پروفایل ویرایش پروفایل ناموفق بود. اجرای دستور "%s" شکست خورد. اجرای ویرایشگر منو با شکست مواجه شد. قفل‌کردن صفحه‌نمایش با شکست مواجه شد. خروج با شکست مواجه شد. بازکردن مدیریت تنظیمات با شکست مواجه شد. تغییر کاربر با شکست مواجه شد. علاقه‌مندی‌ها آیکان آیکان و عنوان اندازه‌ی آیکان آیت_م: بزرگ بزرگ‌تر خر_وج صفحه‌های راهنما منو نا_م: نام هیچ عادی بازکردن آدرس دکمه‌ی پنل الگو قراردادن ورودی _جست‌و‌جو کنار دکمه‌ی پنل قراردادن دسته‌ها درکنار دکمه‌ی پنل قراردادن دستورها کنار _ورودی جست‌و‌جو به تازگی استفاده شده حذف‌کردن از علاقه‌مندی‌ها حذف عمل "%s"؟ حذف عمل انتخاب شده اجرای %s اجراکردن در ترمینال جست‌و‌جوی اعما_ل جست‌و‌جوی اعمال انتخاب یک آیکان انتخاب دستور نمایش یک منو برای دسترسی آسان به برنامه‌های نصب‌شده نمایش برنامه _توضیحات نمایش _نام‌های برنامه‌‌ی عمومی نمایش سلسله‌_مراتب منو کوچک کوچک‌تر مرتب‌سازی براساس حروف الفبا الف-ی مرتب‌سازی براساس حروف الفبا ی-الف تغییر _کاربر تغییر دسته‌ها با _بردن موس روی آن عمل برای همیشه حذف خواهدشد. عنوان ناتوان در اضافه‌کردن لانچر به میزکار. ناتوان در اضافه‌کردن لانچر به پنل. ناتوان در باز کردن این آدرس: %s استفاده از یک _پنل سطری خیلی بزرگ خیلی کوچک منوی Whisker ویکی‌پدیا _ظاهر _رفتار _انصراف _بستن _دستورها _حذف _ویرایش برنامه‌ها _راهنما _آیکان: _قفل‌کردن صفحه‌نمایش _تأیید _الگو: _عبارت منظم _عنوان: ترجمه کننده‌ها 