��    a      $  �   ,      8  
   9     D     S     d     q     u  '   �     �     �     �  2   �  	   	     	     1	  "   E	     h	  	   p	     z	     �	     �	     �	     �	     �	     
     
      0
     Q
  	   i
     s
     x
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
  +     )   2  '   \     �     �     �     �     �     �     �     �            3   &     Z     y     �     �     �     �     �     �               *  '   I     q  "   w      �     �  $   �     �  
     
     
   '     2  	   ?     I  	   U     _     g  	   n     x     �     �     �     �     �  	   �     �     �     �  �  �  
   �     �     �     �     �     �  '   �               %  2   :  	   m     w     �  "   �     �  	   �     �     �     �          #     C     a     x      �     �  
   �     �     �     �     �                 	        (     -     4     9     >     E     N     [  +   c  )   �  '   �     �     �               1     8     H     X     f     u  3   �     �     �     �          '     <     B     J     b     z     �  '   �     �  "   �      �       $   2     W  
   o  
   z  
   �     �  	   �     �  
   �     �     �  	   �     �     �     �     �     �       	             .     6         [   "          9   5   :   =           J   Z   -   a      W      B       .   '   6   >         `         ,   Y   C   ;       Q   &   _   L          8             \       M                           
   S   G       7       ^       (          +   %       I       	               A   T      4                         *   @          N      1       V   H      R      ?       2   #       0   X      O   <                        ]   F       3   P   U   E          K   /      )   $   !       D    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Include _favorites Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-06-23 09:38+0000
Last-Translator: Jason Collins <JasonPCollins@protonmail.com>
Language-Team: English (United Kingdom) (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/en_GB/)
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Add action Add to Desktop Add to Favourites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favourites Icon Icon and title Include _favourites Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favourites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behaviour _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits 