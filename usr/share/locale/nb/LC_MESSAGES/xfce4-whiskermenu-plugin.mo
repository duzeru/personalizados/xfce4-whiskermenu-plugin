��    `        �         (  
   )     4     C     T     a     e  '   s     �     �     �  2   �  	   	     	     !	  "   5	     X	  	   `	     j	     ~	     �	     �	     �	     �	     �	     
       
     A
  	   Y
     c
     h
     w
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
  +   �
  )     '   9     a     o     �     �     �     �     �     �     �     �  3        7     V     q     �     �     �     �     �     �     �       '   &     N  "   T      w     �  $   �     �  
   �  
   �  
          	        &  	   2     <     D  	   K     U     ]     p     v     }     �  	   �     �     �     �  �  �     Y     l     �     �     �     �  (   �     �     �       6   '  
   ^     i     �  "   �     �  	   �     �     �        #     "   3     V     v     �     �     �  
   �     �     �     �                 	   !     +     0     7     <     B  	   I  	   S     ]  (   f  +   �  &   �     �     �               2     ;     L     ]     l     y  ;   �     �     �     �          /     @     F     M     d     {     �  &   �     �  ,   �  '        *      C     d     y     �     �     �  	   �  	   �     �     �     �     �     �     �     �                 
         +     >     F         Z   !          8   4   9   <           I   Y   ,   `      V      A       -   &   5   =         _         +   X   B   :       P   %   ^   K          7             [       L                           
   R   F       6       ]       '          *   $       H       	               @   S      3                         )   ?          M      0       U   G      Q      >       1   "       /   W      N   ;                        \   E       2   O   T   D          J   .      (   #           C    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 19:03+0000
Last-Translator: thomas <thomasbjornvold@gmail.com>
Language-Team: Norwegian Bokmål (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/nb/)
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Legg til aktivitet Legg til på skrivebordet Legg til i favoritter Legg til på panel Alle Alle innstillinger Alternativ appllikasjonsstarter for Xfce Antall enheter Applikasjoner Gjennomsiktighet bakgrunn Surf i filsystemet for å velge en tilpasset kommando. K_ommando: Kateg_ori ikonstørrelse: Fjern sist brukte Copyright © 2013-2019 Graeme Gott Detaljer Vi_sning: Vis siste_brukte som standard Rediger applikasjon... Rediger profil Misslyktes med å redigere profilen Kunne ikke kjøre kommandoen "%s". Kunne ikke starte menyeditoren. Kunne ikke låse skjermen Kunne ikke logge ut Kunne ikke åpne innstillinger Kunne ikke bytte brukere. Favoritter Ikon Ikon og tittel lkonstørrelse: Stor Større Logg ut Man-sider Meny Nav_n: Navn Ingen Normal Åpen URl Paneltast Mønster Plasser _søket ved siden av paneltasten Plasser kategorier ved siden av paneltasten Plasser kommandoer ved siden av søket Siste brukte Fjern fra favoritter Fjerne aktivitet "%s"? Fjerne valgt aktivitet Kjør %s Kjør i terminal Søkeaktiviteter Søkeaktivitet Velg et ikon Velg kommando Vis en meny for enkel tilgang til installerte applikasjoner Vis applikasjonsbeskrivelser Vis applikasjons-verktøytips Vis kategorinavn Vis generiske applikasjons_navn Vis menystruktur Liten Mindre Sorter alfabetisk A-Ø Sorter alfabetisk Ø-A Bytt _brukere Endre kategorier ved mus over Aktiviteten vill bli slettet for godt. Tittel Kunne ikke legge oppstarter til skrivebordet Kunne ikke legge oppstarter til panelet Kan ikke kjøre starter. Kan ikke åpne følgende url: %s Bruk enkel_panel rad Veldig stor Veldig liten Websøk Whisker-meny Wikipedia _Utseende _Oppførsel _Avbryt _Lukk _Kommandoer _Slett _Rediger applikasjoner _Hjelp _Ikon: _Lås skjermen _OK _Mønster: _Regulært uttrykk _Tittel kredit til oversettere 