��    `        �         (  
   )     4     C     T     a     e  '   s     �     �     �  2   �  	   	     	     !	  "   5	     X	  	   `	     j	     ~	     �	     �	     �	     �	     �	     
       
     A
  	   Y
     c
     h
     w
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
  +   �
  )     '   9     a     o     �     �     �     �     �     �     �     �  3        7     V     q     �     �     �     �     �     �     �       '   &     N  "   T      w     �  $   �     �  
   �  
   �  
          	        &  	   2     <     D  	   K     U     ]     p     v     }     �  	   �     �     �     �  �  �     M  '   c      �     �     �     �  )   �          %     4  S   J     �  $   �  -   �  2   �  
   1     <  "   I     l     �  !   �  "   �  *   �          *  *   A  ,   l     �  
   �     �     �     �     �            
         +     2     7     C  
   L     W     i  2   r  1   �  -   �          !      @     a     �     �     �     �     �     �  O   �     N  "   n     �  (   �     �     �     �  #     #   0     T  C   t  ,   �  
   �  >   �  /   /  +   _  4   �  (   �     �     �               0     A     M     ]  	   i     s     �     �  	   �     �     �     �  
   �     �     �              Z   !          8   4   9   <           I   Y   ,   `      V      A       -   &   5   =         _         +   X   B   :       P   %   ^   K          7             [       L                           
   R   F       6       ]       '          *   $       H       	               @   S      3                         )   ?          M      0       U   G      Q      >       1   "       /   W      N   ;                        \   E       2   O   T   D          J   .      (   #           C    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-23 19:01+0000
Last-Translator: Eli Shleifer <eligator@gmail.com>
Language-Team: Hebrew (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/he/)
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 הוספת פעולה הוסף לתוך שולחן עבודה הוסף לתוך מועדפים הוסף לתוך לוח כולם כל _ההגדרות מפעיל יישום חלופי ל-Xfce  כמות _פריטים: יישומים א_טימות רקע: סייר במערכת הקבצים לבחירת פקודה מותאמת אישית. פ_קודה: גודל איקון _קטגוריה: נקה תפריט בשימוש לאחרונה זכויות יוצרים © 2013-2019 Graeme Gott פרטים ת_צוגה: הצג לפי _ברירת מחדל ערוך יישומים... ערוך _פרופיל נכשל לערוך פרופיל. נכשל לבצע פקודה "%s". נכשל להפעיל עורך תפריט. נכשל לנעול מסך. נכשל להתנתק. נכשל לפתוח מנהל הגדרות. נכשל להעביר בין משתמשים. מועדפים איקון איקון וכותרת גודל איקון _פריט: גדול גדול יותר הת_נתק עמודי Man תפריט _שם: שם אף אחד רגיל פתח URI כפתור לוח דפוס מקם ערך _חיפוש ליד כפתור לוח מקם ק_טגוריות לצד כפתור לוח מקם פקודות ליד _ערך חיפוש בשימוש לאחרונה הסר מתוך מועדפים להסיר את פעולה "%s"? הסרת פעולה נבחרת הפעל %s הפעל בתוך מסוף פעו_לות חיפוש פעולת חיפוש בחר איקון בחירת פקודה הצג תפריט כדי לגשת בקלות אל יישומים מותקנים הצג _תיאורי יישום הצג תיבת _עזר יישום הצג שמות_קטגוריה הצג שמות יישום _כלליים הצג מ_דרג תפריט קטן קטן יותר מיין לפי אלף-בית א-ת מיין לפי אלף-בית ת-א העבר בין _משתמשים העבר בין קטגוריות על ידי _מיקום עליהן הפעולה תימחק באופן קבוע. כותרת לא יכול להוסיף מפעיל לשולחן עבודה. לא יכול להוסיף מפעיל ללוח. לא ניתן לערוך את המפעיל. לא יכול לפתוח כתובת url הבאה: %s השתמש בשורת _לוח בודדת גדול מאוד קטן מאוד חיפוש רשת תפריט Whisker ויקיפדיה הו_פעה הת_נהגות _ביטול _סגור _פקודות _מחק _ערוך יישומים _עזרה אי_קון: _נעל מסך _אישור _דפוס: _ביטוי רגולרי _כותרת: שבחים-מתרגמים 