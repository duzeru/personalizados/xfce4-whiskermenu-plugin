��    \      �     �      �  
   �     �     �                 '   #     K     ]     j  2     	   �     �     �  "   �     	  	   	     	     .	     <	     T	     t	     �	     �	      �	     �	  	   �	     �	     
     
     $
     *
     1
  	   :
     D
     I
     P
     U
     Z
     a
     j
     w
  +   
  '   �
     �
     �
     �
          "     )     9     I     W     f  3   u     �     �     �                    &     >     V     d  '   �     �  "   �      �  $   �       
   2  
   =  
   H     S  	   `     j  	   v     �     �  	   �     �     �     �     �     �     �  	   �     �     �     �  �       �     �     �     �     �     �  ,   �          1     >  E   S  	   �     �     �  "   �     �  
             !     0  "   J  #   m     �     �  )   �     �  	                   0     J     Q     ]     n     {     �     �     �     �  	   �     �     �  1   �  (   �          &     >     Y     v     �     �     �     �     �  E   �  $     +   D  '   p     �     �     �     �     �       '         @     a  +   i  &   �  #   �     �  	   �       	          	   '     1     :  
   J     U  	   ]     g     s     �     �     �     �  	   �     �  	   �  )   �     E      ,   "   9   /   '   =          P   8      F               X   K              V   B   *      +       Q       L   J   0   :   N      ;      \   (   !   >                 7       W           D   3   A   O   U   C      <                %   5          [       G   T       @       -                                    I   4   Z       1   &   Y   .           	         $       6   2                        S      M          H      
             R   ?       #              )    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-09-20 23:37+0000
Last-Translator: enolp <enolp@softastur.org>
Language-Team: Asturian (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/ast/)
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Amestar aición Amestar al escritoriu Amestar a favoritos Amestar al panel Too Tolos axuste_s Llanzador d'aplicaciones alternativu pa Xfce Cantidá d'_elementos: Aplicaciones _Opacidá del fondu: Restolar pel sistema de ficheros o esbillar un comandu personalizáu. C_omandu: Tamañ_u d'iconu d'estaya: Llimpiar Usáo apocayá Copyright © 2013-2019 Graeme Gott Detalles Pa_ntalla: Amosar por _defeutu Editar _perfil Fallu al abrir el perfil. Fallu al executar el comandu "%s". Fallu al llanzar l'editor de menú. Fallu al bloquiar la pantalla. Fallu al zarrar sesión. Fallu al abrir l'alministrador d'axustes. Fallu al cambear d'usuarios. Favoritos Iconu Iconu y títulu Ta_mañu d'iconu d'oxetu: Grande Más grande Zarrar _sesión: Páxines man Menú Nom_e: Nome Nada Normal Abrir URI Botón del panel Patrón Allugar el cuadru de _gueta co'l botón del panel Allugar los comandos co la caxa de gueta Usáu apocayá Desaniciar de favoritos ¿Desaniciar aición "%s"? Desaniciar aición esbillada Executar %s Executar na terminal Guetar aicio_nes Guetar aición Esbilla un iconu Esbillar comandu Amuesa un menú p'acceder cenciellamente a les aplicaciones instalaes Amosar descripciones _d'aplicaciones _Amodsar conseyos emerxentes d'aplicaciones Amosar _nomes d'aplicaciones xenéricos Amosar xe_rarquía del menú Pequeñu Más pequeñu Ordenar alfabéticamente A-Z Ordenar alfabéticamente Z-A Cambear _usuarios Camudar ente estayes _apuntando col mur L'aición desanciaráse dafechu. Títulu Nun pue amestase'l llanzador al escritoriu. Nun pue amestase'l llanzador al panel. Nun pue abrise la URL siguiente: %s Usar panel d'una _filera Pergrande Perpequeñu Gueta web Menú Whisker Wikipedia _Aspeutu _Comportamientu _Encaboxar _Zarrar _Comandos _Desaniciar _Editar aplicaciones _Ayuda _Iconu: _Bloquiar pantalla _Aceutar _Patrón: Espresión _regular _Títulu: Softastur <alministradores@softastur.org> 