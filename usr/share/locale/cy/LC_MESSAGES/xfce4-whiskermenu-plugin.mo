��    b      ,  �   <      H  
   I     T     c     t     �     �  '   �     �     �     �  2   �  	   "	     ,	     A	  "   U	     x	  	   �	     �	     �	     �	     �	     �	     �	     
     -
      @
     a
  	   y
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
            +     )   B  '   l     �     �     �     �     �     �     �     
          '  3   6     j     �     �     �     �     �     �     �           ,     M     [  '   z     �  "   �      �     �  $        *  
   B  
   M  
   X     c  	   p     z  	   �     �     �  	   �     �     �     �     �     �     �  	   �     �             �       �     �     �     �     
               =  	   O     Y  !   n  
   �     �     �  #   �     �     �               *     >     V  "   u     �     �  !   �     �  
   �                    *     A  	   F  
   P  
   [     f     o     t     x     |     �     �     �  )   �  *   �  '   �          0     E     \  	   q     {     �     �     �     �  .   �          +     G  "   `     �     �  	   �     �     �  "   �     �  (   �  &   #     J  *   P  $   {     �     �     �     �     �            	   !     +  	   1     ;     C     G     S     Y     j     r     y     �     �     �     �     �         \   "          9   5   :   =           K   [   -   b      X      B       .   '   6   >         a         ,   Z   C   ;       R   &   `   M          8             ]       N                           
   T   H       7       _       (          +   %       J       	               A   U      4                         *   @          O      1       W   I      S      ?       2   #   Y   0   G      P   <                        ^   E       3   Q   V   F          L   /      )   $   !       D    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Include _favorites Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Stay _visible when focus is lost Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-07-16 19:17+0000
Last-Translator: ciaran
Language-Team: Welsh (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/cy/)
Language: cy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n==2) ? 1 : (n != 8 && n != 11) ? 2 : 3;
 Ychwanegu gweithred Ychwanegu i'r bwrdd gwaith Ychwanegu i ffefrynnau Ychwanegu i banel Popeth Pob gosodiad Lansiwr rhaglenni amgen i Xfce Nifer yr eitemau: Rhaglenni Gwelededd y cefndir: Pori i ddewis gorchymyn addasedig Gorchymyn: Maint eiconau categorïau Clirio'r eitemau diweddar Hawlfraint © 2013-2019 Graeme Gott Manylion Dangos: Dangos yn ddiofyn Golygu'r rhaglen... Golygu eich proffil Methu golygu'r proffil. Methu rhedeg y gorchymyn "%s". Methu lansio golygydd y ddewislen. Methu cloi'r sgrin. Methu allgofnodi. Methu agor y rheolydd gosodiadau. Methu newid defnyddwyr Ffefrynnau Eicon Eicon a theitl Cynnwys ffefrynnau Maint eiconau eitemau: Mawr Mawr iawn Allgofnodi Canllawiau Dewislen Enw: Enw Dim Canolig Agor URI Botwm y panel Patrwm Lleoli chwiliadau wrth ymyl botwm y panel Lleoli categorïau wrth ymyl botwm y panel Lleoli gorchmynion wrth ymyl chwiliadau Eitemau diweddar Tynnu o'r ffefrynnau Tynnu'r weithred "%s"? Tynnu'r weithred hon Rhedeg %s Rhedeg mewn terfynell Gweithredoedd chwilio Gweithredoedd chwilio Dewiswch eicon Dewiswch orchymyn Dewislen i gael hyd at eich rhaglenni yn hawdd Dangos disgrifiadau rhaglenni Dangos trosolygon rhaglenni Dangos enwau categorïau Dangos enwau cyffredinol rhaglenni Dangos hierarchaethau Bach Bach iawn Trefnu o A i Y Trefnu o Y i A Cadw'n weladwy pan gollir y ffocws Newid defnyddwyr Newid categorïau drwy hofran y llygoden Caiff y gorchymyn ei ddileu'n barhaol. Teitl Methu ychwanegu'r lansiw i'r bwrdd gwaith. Methu ychwanegu'r lansiwr i'r panel. Methu golygu'r lansiwr. Methu agor yr URL canlynol: %s Defnyddio ond un rhes o'r panel Enfawr Pitw Chwilio'r we Dewislen Whisker Wicipedia Golwg Ymddygiad Diddymu Cau Gorchmynion Dileu Golygu rhaglenni Cymorth Eicon: Cloi'r sgrin Iawn Patrwm: RegEx Teitl: Cyfieithwyd gan . 