��    N      �  k   �      �  
   �     �     �     �     �     �  2   �  	   &     0     E     Y  	   a     k     �     �     �      �     �  	                  *     ;     A     H  	   Q     [     `     g     l     q     x     �     �  +   �  '   �     �     �     	     "	     9	     @	     P	     `	     n	     }	  3   �	     �	     �	     �	     
     
     "
     :
     R
     `
  '   
     �
  "   �
      �
  
   �
  
   �
       	          	   *     4     <     C     K     ^     d     k     x  	   |     �     �  �  �     0     D     W     m  
   }     �  ;   �     �  -   �  +     
   <     G  $   V  6   {  "   �     �  /   �  %   %  
   K     V     f  0   �     �  
   �     �     �     �     �     �     �                    0  3   6  -   j     �     �     �     �  	   �     �          3     9     P  >   d  "   �  /   �  3   �     *  
   0     ;     [     {  ,   �  )   �     �  ;   �  9   9     s          �  	   �     �     �     �     �  
   �     �     �     �          #     '     .     F            *       ,          (           L   I             !   8      K      H   6   %      N   E   7   M   =         J      5   F   <              &   /   ?          G   3                      1   	   )       2          >   -       :   
      #       @             $      B   C              A       +            "   4      D   .                         ;           '         9              0        Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Details Di_splay: Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Very Large Very Small Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-05-05 02:44+0000
Last-Translator: Duy Truong Nguyen <truongap.ars@gmail.com>
Language-Team: Vietnamese (http://www.transifex.com/projects/p/xfce4-whiskermenu-plugin/language/vi/)
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Thêm hành động Thêm vào Desktop Thêm vào Ưa Thích Thêm và Khung Tất cả Tất cả cài đặt Duyệt hệ thống để chọn một lệnh tùy thích. Lệnh Kích thước biểu tượng phân l_oại: Dọn danh sách được dùng gần đây Chi tiết H_iển thị: Thi hành lệnh "%s", thất bại. Không thể chạy trình chỉnh sửa thực đơn. Thất bại khi khóa màn hình. Thất bại khi đăng xuất. Thất bại khi mở quản lý thiết lập. Chuyển người dùng thất bại. Ưa Thích Biểu tượng Tiêu đề và biểu tượng Kí_ch thước biểu tượng đối tượng: Lớn Lớn hơn Đăng xuất Trang chính Thực đơn Tên Tên Trống Bình thường Mở URI Nút điều khiển Mẫu Đặt ô tìm kiếm gần biểu tượng Whisker Đặt các nút lệnh cạnh ô tìm kiếm Truy cập gần đây Loại bỏ khỏi Ưa Thích Xóa bỏ hành động %s? Hủy bỏ  Chạy %s Chạy trong dòng lệnh Tìm kiếm hành động Tìm  Chọn biểu tượng Lựa chọn lệnh Hiển thị thực đơn để chạy ứng dụng dễ dàng Hiển thị mô tả_ứng dụng Hiển thị bằng tên chung cho ứng dụng Hiểu thị thực đơn theo thứ tự ưu tiên Nhỏ Nhỏ hơn Sắp xếp theo thứ tự A-Z Sắp xếp theo thứ tự Z-A Chuyển đổi người dùng Chuyển đổi tiêu đề khi rê ch_uột Hành động sẽ bị xóa vĩnh viễn Tiêu đề Không thể thêm vào thanh truy cập nhanh ở desktop. Không thể thêm vào thanh truy cập nhanh hay khung. Rất lớn Rất nhỏ Thực đơn Whisker Wikipedia _Diện mạo _Thói quen _Thôi Đón_g _Xóa bỏ _Sửa Ứng dụng Trợ g_iúp _Biểu tượng: _Khóa màn hình _OK _Mẫu _Quy tắc thể hiện _Tiêu đề: 