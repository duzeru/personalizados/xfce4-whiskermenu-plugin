��    J      l  e   �      P  
   Q     \     k     |     �     �     �  2   �  	   �     �     �       	              @     ^     u      �     �  	   �     �     �     �     �     �     �  	                       !     &  +   -  '   Y     �     �     �     �     �     �     �     �     	     	  3   #	     W	     v	     |	     �	     �	     �	     �	  '   �	     	
  "   
      2
  
   S
  
   ^
  
   i
     t
  	   �
     �
  	   �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
  �  �
     �  !         "     A     \     a     z  @   �     �      �  $   �          *  "   3  *   V     �     �  *   �  #   �               *  "   E     h     q     z     �  
   �     �     �     �     �  5   �  2        A     [     x     �  	   �     �     �     �     �       V     !   t     �     �     �     �     �  0   �  &   )  
   P  C   [  <   �     �     �     �          &  	   9  	   C  
   M  	   X  
   b     m     u     �     �     �     �     �     A       J   .   I                 
      *   8           >   (                   0       +   D         2                  E   ?   5          :             <              1   #   @       7          6   !              H   &   ,      9   G       F   	   =      ;      C               %   4         -   $                        /          "   )                3   B   '        Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Applications Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Details Di_splay: Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Position _search entry next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Title: Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-03-03 15:49+0000
Last-Translator: محمدأمين الصامت <mohamedamin.samet@gmail.com>
Language-Team: Arabic (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/ar/)
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 أضف إجراء أضف إلى سطح المكتب أضف إلى المفضلات أضف إلى الشريط كل كل الإعدا_دات التطبيقات تصفح نظام الملفات لإختيار أمر مخصص. أ_مر: _حجم أيقونة الصنف: محو المستعملة حديثا تفاصيل عر_ض: فشل تنفيذ الأمر "%s". فشل إطلاق محرر القائمة. فشل إغلاق الشاشة. فشل تسجيل الخروج. فشل فتح مدير الإعدادات. فشل تبديل المستخدم. مفضلات أيقونة أيقونة و عنوان ح_جم أيقونة العنصر: كبير أكبر ت_سجيل الخروج صفحات الدليل قائمة _إسم: إسم بدون عادي موضع مربع البحث يلي زر الشريط موضع الأوامر يلي مربع البحث مستعملة حديثا حذف من المفضلات حذف الإجراء "%s"? حذف شغل %s شغل في الطرفية بح_ث عن الإجراءات بحث عن إجراء إختر أيقونة حدد أمرا إظهار قائمة لولوج إلى التطبيقات المثبتة بسهولة عرض _وصف التطبيقات صغير أصغر رتب أبجديا أ-ي رتب أبجديا ي-أ تغيير المستخدم تبديل الأصناف ح_سب الترفيف الإجراء سيمسح نهائيا عنوان عاجز عن إضافة المطلقة إلى سطح المكتب. عاجز عن إضافة المطلقة إلى الشريط. كبير جدا صغير جدا بحث في الويب قائمة Whisker ويكيبيديا _مظهر س_لوك إلغاء أ_غلق أوامر _مسح _تعديل التطبيقات _مساعدة _أيقونة: قفل الشاشة _موافق عن_وان: 