��    d      <  �   \      �  
   �     �     �     �     �     �  '   �     �     	  !   	  !   4	  #   V	     z	  2   �	  	   �	     �	     �	  "   �	     
  	    
     *
     >
     R
     `
     x
     �
     �
     �
      �
       	        #     (     7     J     [     a     h  	   q     {     �     �     �     �     �     �     �  +   �  )   �  '        4     B     X     l     �     �     �     �     �     �  3   �     
     )     D     Y     y     �     �     �     �     �     �  '   �     !  "   '      J     k  $   �     �  
   �  
   �  
   �     �  	   �     �  	               	        (     0     C     I     P     ]  	   a     k          �  �  �     &     :     U     l     �     �  $   �     �     �  '   �  (   �  )   (     R  =   i  
   �     �     �  "   �                    %     9  %   J  +   p  +   �  $   �     �  4     '   A  	   i     s     x     �     �     �     �  	   �     �     �     �     �     �     �  
   �  
   �       (     '   :  &   b     �     �     �     �     �     �     �     
          %  ;   4     p      �     �     �     �     �     �     �          0  )   C  )   m     �  #   �  1   �  +   �  !        A     _     k     x     �  	   �  	   �  	   �     �     �  
   �     �     �     �     �     �       
             $  &   ,             A   ,   K          Q                d                -   %   F       #   &   E           M               )      .   8   N   U   =   J      `   !      /       W   *   P       [       T              <   b   (         @   I   B   4   >             S   Y       _   "           ;                     1           $      7          
   5           3   L   a              9   6      G   +   :       c   X              D   V   C         \   2      ?   '   Z             O       ]   ^   R          H   	           0    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Are you sure you want to log out? Are you sure you want to restart? Are you sure you want to shut down? Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Include _favorites Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-03-13 07:02+0000
Last-Translator: Carlos Dz <cls567@tuta.io>
Language-Team: Swedish (Sweden) (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/sv_SE/)
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Lägg till åtgärd Lägg till på skrivbordet Lägg till i favoriter Lägg till i panelen Alla Alla _inställningar Alternativ programstartare för Xfce Antal _objekt: Program Är du säker på att du vill logga ut? Är du säker på att du vill starta om? Är du säker på att du vill stänga av? Bakgrundens opa_citet: Bläddra i filsystemet för att välja ett anpassat kommando. K_ommando: K_ategori ikonstorlek: Rensa senast använda Copyright © 2013-2019 Graeme Gott Detaljer _Visa: Visa som _standard Redigera program... Redigera _profil Misslyckades med att redigera profil. Misslyckades med att utföra kommando "%s". Misslyckades med att starta menyredigerare. Misslyckades med att låsa skärmen. Misslyckades med att logga ut. Misslyckades med att öppna inställningshanteraren. Misslyckades med att växla användare. Favoriter Ikon Ikon och titel Inkludera _favoriter _Objekt ikonstorlek: Stor Större L_ogga ut Manualsidor Meny _Namn: Namn Ingen Normal Öppna URI Panelknapp Mönster Placera s_ökfältet intill panelknappen Placera k_ategorier intill panelknappen Placera ko_mmandon bredvid sökfältet Senast använda Ta bort från favoriter Ta bort åtgärd "%s"? Ta bort markerad åtgärd Kör %s Kör i terminal Sök_åtgärder Sökåtgärd Välj en ikon Välj kommando Visa en meny för att enkelt komma åt installerade program Visa _programbeskrivningar Visa _verktygstips _för program Visa kate_gorinamn Visa allmänna program_namn Visa _hierarkisk meny Liten Mindre Sortera alfabetiskt A-Ö Sortera alfabetiskt Ö-A _Växla användare _Växla kategorier med drag av muspekaren Åtgärden kommer att tas bort permanent. Titel Kunde inte skapa skrivbordsgenväg. Kunde inte lägga till programstartare i panelen. Det gick inte att redigera programstartare. Kan inte öppna följande URL: %s Använd en _enskild panel rad Mycket stor Mycket liten Webbsökning Whisker Meny Wikipedia _Utseende _Beteende _Avbryt _Stäng _Kommandon Ta bort _Redigera program _Hjälp _Ikon: _Lås skärm _OK _Mönster: _Reguljära uttryck Tite_l: Patrik Nilsson <translation@hembas.se> 