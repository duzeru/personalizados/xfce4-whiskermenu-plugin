��    w      �  �   �      
  
   
     $
     3
     D
     Q
     U
  '   c
     �
     �
  !   �
  !   �
  #   �
       2   '  	   Z     d     y  "   �     �  	   �     �     �     �          )     7     O     o     �     �     �      �     �                (  	   @  #   J     n     s     �     �     �     �     �     �     �  	   �     �     �     �     �          
             +   (  )   T  '   ~     �     �     �     �  "   �               /     ?     M     \     k  3   |     �     �     �               9  
   N     Y     _     g           �     �  "   �     �     �  '        9  #   ?  "   c      �     �  $   �     �  
   �  
     
          	   +     5  	   A     K     S  	   Z     d     l       
   �     �     �     �  	   �     �     �     �     �  �  �     m     }     �     �     �     �  *   �            &   #  #   J  "   n  "   �  E   �  
   �          %  %   B     h     w       !   �  !   �     �     �     �  !         :     [     z     �  &   �     �     �      �        
   =  7   H  	   �     �     �     �     �     �     �  	   �  "   �          !     &     -     2     7     ?     H     U  !   ]  &        �     �     �     �     
  ,   "     O     X  
   j  	   u          �     �  E   �  %   �           ?     X     t     �  
   �     �     �     �     �  .   �     -  :   9     t  :   �  (   �     �  1   �  *   %  (   P     y  )   �     �  
   �  
   �     �       	     
        #  	   +     5     >     J     R     i  
   o     z     �     �  	   �     �     �     �     �         H   n   )   3      .       w   2   @           [   Q   B                  k   \   F   +       =   6   i   v      o                  0   s   q   #   g   f   9   ?          G   b   j   1   <       S   P       	             l      Y       D   E   C      U   L                 T   /   (              _             A                                     J           m   d       u   -       `         a   X           :   '       
      I   %      ^   Z      N      *   t               M   r   5   !   4   h                  R           >   e   c           V   O       8      $   ,       K   ;   W   &   p   7   ]       "    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Are you sure you want to log out? Are you sure you want to restart? Are you sure you want to shut down? Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Do you want to suspend to RAM? Do you want to suspend to disk? Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to hibernate. Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to restart. Failed to shut down. Failed to suspend. Failed to switch users. Favorites Hibernating computer in %d seconds. Icon Icon and title Include _favorites Ite_m icon size: Large Larger Log Ou_t... Log _Out Logging out in %d seconds. Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Restarting computer in %d seconds. Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Session Commands Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show c_onfirmation dialog Show cate_gory names Show generic application _names Show menu hie_rarchy Shut _Down Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Stay _visible when focus is lost Suspe_nd Suspending computer in %d seconds. Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Turning off computer in %d seconds. Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Hibernate _Icon: _Lock Screen _OK _Pattern: _Regular expression _Restart _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-09-05 10:25+0000
Last-Translator: Pjotr <pjotrvertaalt@gmail.com>
Language-Team: Dutch (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/nl/)
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Actie toevoegen Voeg toe aan bureaublad Voeg toe aan favorieten Voeg toe aan werkbalk Alle Alle _instellingen Alternatieve toepassingenstarter voor Xfce Hoeveelheid _elementen: Menu Weet u zeker dat u zich wilt afmelden? Weet u zeker dat u wilt herstarten? Weet u zeker dat u wilt afsluiten? Ondoorzichtigheid van achtergrond: Blader door het bestandssysteem om een aangepaste opdracht te kiezen. _Opdracht: Pictogramgrootte van categorie: Verwijder 'onlangs gebruikt' Auteursrecht © 2013-2019 Graeme Gott Bijzonderheden _Tonen: _Standaard tonen Wilt u de pauzestand inschakelen? Wilt u de slaapstand inschakelen? Toepassing bewerken... _Profiel bewerken Kon profiel niet bewerken. Kon opdracht '%s' niet uitvoeren. Kon slaapstand niet inschakelen. Kon menubewerker niet starten. Kon scherm niet vergrendelen. Kon niet afmelden. Kon instellingenbeheerder niet openen. Kon niet herstarten. Kon niet afsluiten. Kon pauzestand niet inschakelen. Kon niet van gebruiker wisselen. Favorieten Computer wordt over %d seconden in slaapstand gebracht. Pictogram Pictogram en titel Favorieten opnemen Pictogramgrootte van element: Groot Groter A_fmelden... Af_melden U wordt over %d seconden afgemeld. Handleiding Menu _Naam: Naam Geen Normaal Open URI Werkbalkknop Patroon Plaats zoekvak naast werkbalkknop Plaats categorieën naast werkbalkknop Plaats opdrachten naast zoekvak Onlangs gebruikt Verwijderen uit favorieten Actie '%s' verwijderen? Verwijder gekozen actie De computer wordt over %d seconden herstart. Draai %s Draai in terminal Zoekacties Zoekactie Pictogram kiezen Opdracht kiezen Sessie-opdrachten Toon een menu om geïnstalleerde toepassingen eenvoudig op te starten Toon _beschrijvingen van toepassingen Toon zweeftips voor toepassingen Toon bevestigingsdialoog Toon namen van categorieën Toon generieke toepassingsnamen Toon opbouw van menu Af_sluiten Klein Kleiner Rangschik alfabetisch van A-Z Rangschik alfabetisch van Z-A Blijf _zichtbaar wanneer scherpstelling weg is _Pauzestand De computer wordt over %d seconden in pauzestand gebracht. Gebruikers _wisselen Verwissel van categorie door muispijl erop te laten rusten De actie zal blijvend worden verwijderd. Titel De computer wordt over %d seconden uitgeschakeld. Kon starter niet toevoegen aan bureaublad. Kon starter niet toevoegen aan werkbalk. Kon starter niet bewerken. Kon het volgende webadres niet openen: %s Gebruik een enkele werkbalkrij Heel groot Heel klein Webdoorzoeking Whiskermenu Wikipedia _Uiterlijk _Gedrag _Afbreken _Sluiten Op_drachten _Wissen Toepassingen _bewerken _Hulp Slaapstand P_ictogram: Scherm _vergrendelen _Oké _Patroon: _Reguliere uitdrukking _Herstarten _Titel: Pjotr <pjotrvertaalt@gmail.com> 