��    w      �  �   �      
  
   
     $
     3
     D
     Q
     U
  '   c
     �
     �
  !   �
  !   �
  #   �
       2   '  	   Z     d     y  "   �     �  	   �     �     �     �          )     7     O     o     �     �     �      �     �                (  	   @  #   J     n     s     �     �     �     �     �     �     �  	   �     �     �     �     �          
             +   (  )   T  '   ~     �     �     �     �  "   �               /     ?     M     \     k  3   |     �     �     �               9  
   N     Y     _     g           �     �  "   �     �     �  '        9  #   ?  "   c      �     �  $   �     �  
   �  
     
          	   +     5  	   A     K     S  	   Z     d     l       
   �     �     �     �  	   �     �     �     �     �  q  �     \     m     �     �     �     �  $   �     �  
   �  %     &   .  &   U     |  1   �  
   �     �     �  "        )     2     8     J     e     �     �     �  #   �     �  !         "     =  )   R     |     �     �     �  
   �  '   �                    4     Q     V     ^     j     r  	   �     �     �     �     �     �     �  	   �     �  /   �  )      0   *     [     k     �     �  %   �     �     �     �     �     
          '  9   :     t     �     �     �     �     �                         0  !   H     j  &   q     �  *   �  '   �     �  "   �  -   "  (   P     y  $   �     �  
   �     �     �     �  	     	     
     	   $     .     3     ?     E     Y     a     h     o     |  
   �     �  	   �     �     �         H   n   )   3      .       w   2   @           [   Q   B                  k   \   F   +       =   6   i   v      o                  0   s   q   #   g   f   9   ?          G   b   j   1   <       S   P       	             l      Y       D   E   C      U   L                 T   /   (              _             A                                     J           m   d       u   -       `         a   X           :   '       
      I   %      ^   Z      N      *   t               M   r   5   !   4   h                  R           >   e   c           V   O       8      $   ,       K   ;   W   &   p   7   ]       "    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Are you sure you want to log out? Are you sure you want to restart? Are you sure you want to shut down? Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Do you want to suspend to RAM? Do you want to suspend to disk? Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to hibernate. Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to restart. Failed to shut down. Failed to suspend. Failed to switch users. Favorites Hibernating computer in %d seconds. Icon Icon and title Include _favorites Ite_m icon size: Large Larger Log Ou_t... Log _Out Logging out in %d seconds. Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Restarting computer in %d seconds. Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Session Commands Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show c_onfirmation dialog Show cate_gory names Show generic application _names Show menu hie_rarchy Shut _Down Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Stay _visible when focus is lost Suspe_nd Suspending computer in %d seconds. Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Turning off computer in %d seconds. Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Hibernate _Icon: _Lock Screen _OK _Pattern: _Regular expression _Restart _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-28 15:49+0000
Last-Translator: scootergrisen
Language-Team: Danish (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/da/)
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Tilføj handling Tilføj til skrivebord Tilføj til favoritter Tilføj til panel Alle Alle _indstillinger Alternativ programopstarter til Xfce Antal _elementer: Programmer Er du sikker på, at du vil logge ud? Er du sikker på, at du vil genstarte? Er du sikker på, at du vil lukke ned? Baggrundens _opacitet: Gennemse filsystemet for at vælge egen kommando. K_ommando: Ikonstørrelse for _kategori: Nulstil senest anvendte Ophavsret © 2013-2019 Graeme Gott Detaljer _Vis: Vis som _standard Vil du suspendere til RAM? Vil du suspendere til disk? Rediger program ... Rediger _profil Profilen kunne ikke redigeres. Kunne ikke udføre kommandoen "%s". Kunne ikke gå i dvale. Kunne ikke starte menuredigering. Kunne ikke låse skærmen. Kunne ikke logge ud. Kunne ikke åbne indstillingshåndtering. Kunne ikke genstarte. Kunne ikke lukke ned. Kunne ikke suspendere. Kunne ikke skifte bruger. Favoritter Computeren går i dvale om %d sekunder. Ikon Ikon og titel Inkluder _favoritter Ikonstørrelse for _element: Stor Større Log _ud ... Log _ud Logger ud om %d sekunder. Man-sider Menu Na_vn: Navn Ingen Normal Åbn URI Panelknap Mønster Placer _søgeindtastning ved siden af panelknap Placer _kategorier ved siden af panelknap Placer kommandoer ved siden af søge_indtastning Senest anvendte Fjern fra favoritter Fjern handlingen "%s"? Fjern valgte handling Computeren genstartes om %d sekunder. Kør %s Kør i terminal Søgeha_ndlinger Søgehandling Vælg et ikon Vælg kommando Sessionskommandoer Vis en menu for hurtig adgang til installerede programmer Vis program_beskrivelser Vis program_værktøjstips Vis _bekræftelsesdialog Vis kate_gorinavne Vis generiske program_navne Vis menuhie_rarki _Luk ned Lille Mindre Sortér alfabetisk A-Å Sortér alfabetisk Å-A Forbliv _synlig når fokus mistes _Hvile Computeren suspenderes om %d sekunder. Skift _bruger Skift kategorier ved at flytte musemarkør Handlingen vil blive slettet permanent. Titel Computeren slukkes om %d sekunder. Kan ikke tilføje opstarter til skrivebordet. Kan ikke tilføje opstarter til panelet. Kan ikke redigere opstarter. Kan ikke åbne den følgende URL: %s Brug én række i _panelet Meget stor Meget lille Websøgning Whiskermenu Wikipedia _Udseende _Opførsel _Annuller _Luk _Kommandoer _Slet _Rediger programmer _Hjælp _Dvale _Ikon: _Lås skærm _OK _Mønster: _Regulært udtryk _Genstart _Titel: scootergrisen
EpsilonDK 