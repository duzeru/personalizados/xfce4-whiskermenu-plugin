��    b      ,  �   <      H  
   I     T     c     t     �     �  '   �     �     �     �  2   �  	   "	     ,	     A	  "   U	     x	  	   �	     �	     �	     �	     �	     �	     �	     
     -
      @
     a
  	   y
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
            +     )   B  '   l     �     �     �     �     �     �     �     
          '  3   6     j     �     �     �     �     �     �     �           ,     M     [  '   z     �  "   �      �     �  $        *  
   B  
   M  
   X     c  	   p     z  	   �     �     �  	   �     �     �     �     �     �     �  	   �     �             �       �          /     L     i     p  D   �     �     �  '   �  m   '     �  ,   �  .   �  N        Q     h  '   w     �     �  5   �  B     R   N  7   �  2   �  N     ?   [     �     �     �     �  .   �     $     1     :     H     f     w     �  
   �     �     �     �     �  O   �  ?   0  @   p  !   �  "   �  2   �  (   )     R  $   d     �     �  !   �  !   �  f     -   m  6   �  %   �  @   �  2   9     l     u  #   �  #   �  9   �  "     4   )  9   ^     �  R   �  I   �  9   B  I   |  .   �     �               9     U     j     x     �     �     �     �     �     �     �               ,     =     T  3   c         \   "          9   5   :   =           K   [   -   b      X      B       .   '   6   >         a         ,   Z   C   ;       R   &   `   M          8             ]       N                           
   T   H       7       _       (          +   %       J       	               A   U      4                         *   @          O      1       W   I      S      ?       2   #   Y   0   G      P   <                        ^   E       3   Q   V   F          L   /      )   $   !       D    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Include _favorites Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Stay _visible when focus is lost Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-07-09 07:55+0000
Last-Translator: Саша Петровић <salepetronije@gmail.com>
Language-Team: Serbian (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/sr/)
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Додај радњу Додај на површ Додај у омиљене Додај на полицу Све Све _поставке Заменски покретач програма за Иксфце Количина _ставки: Програми Прозирност позадине_: Разгледај систем датотека ради одабира произвољне наредбе. Н_аредба: Величина иконица _врста: Очисти недавно коришћено Права умножавања © 2013-2019 Грејем Гот (Graeme Gott) Појединости Пр_иказ: Подразумевани приказ Уреди програм... Уреди _профил Нисам успео да уредим профил. Нисам успео да извршим наредбу „%s“. Нисам успео да покренем уређивача изборника. Нисам успео да закључам екран. Нисам успео извршим одјаву. Нисам успео да отворим управника поставки. Нисам успео да променим корисника. Омиљено Сличица Сличица и наслов Укључи _омиљене Величина иконице став_ке: Велике Веће Одја_ва Упутне странице Изборник И_ме: Име Ништа Обичне Отвори адресу Дугме полице Образац Постави кућицу за унос до дугмета на полици Постави врсте до дугмета на полици Постави наредбе до кућице претраге Недавно коришћено Уклони из омиљених Да ли да уклоним радњу „%s“? Уклони означену радњу Покрени %s Покрени у терминалу Р_адње претраге Радње претраге Изаберите иконицу Изаберите наредбу Приказуј изборник за брзи приступ уграђеним програмима Приказуј _описе програма Приказуј програмске напомене Прикажи имена врста_ Приказуј уопштене _називе програма Приказуј гранање изборника Мале Малецне Поређај азбучно А-Ш Поређај азбучно Ш-А Остани _видљив при губитку жиже Промени _корисника Мењај врсте _надношењем миша Радња ће бити трајно избрисана. Наслов Нисам успео да додам покретач на радну површ. Нисам успео да додам покретач на полицу. Нисам успео да уредим покретач. Нисам успео да отворим следећу адресу: %s Користи један ред _површи Врло велике Врло мале Претрага мреже Бркин изборник Википедија _Изглед _Понашање _Откажи _Затвори _Наредбе _Избриши _Уреди програме _Помоћ _Иконица: _Закључај екран _У реду _Образац: Општи из_раз _Наслов: Саша Петровић <salepetronije@gmail.com> 