��    a      $  �   ,      8  
   9     D     S     d     q     u  '   �     �     �     �  2   �  	   	     	     1	  "   E	     h	  	   p	     z	     �	     �	     �	     �	     �	     
     
      0
     Q
  	   i
     s
     x
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
     �
  +     )   2  '   \     �     �     �     �     �     �     �     �            3   &     Z     y     �     �     �     �     �     �               *  '   I     q  "   w      �     �  $   �     �  
     
     
   '     2  	   ?     I  	   U     _     g  	   n     x     �     �     �     �     �  	   �     �     �     �    �       +     &   I     p     �     �  O   �  '   �     #  !   4  i   V     �  ,   �  !   �  "        ?     Z  6   w  (   �  $   �  B   �  =   ?  G   }  <   �  '     G   *  D   r     �     �  "   �  &   �  *   #  
   N     Y     f     s     �     �  
   �     �     �     �     �     �  .     7   1  C   i     �  *   �  %   �  .        >  (   T     }     �     �     �  s   �  :   \  X   �  9   �  D   *  4   o     �     �  6   �  6   
  *   A  ]   l  >   �     	  X     K   u  ;   �  6   �  F   4     {     �     �     �     �     �     �                0     >  &   P     w     �  "   �     �     �     �     �  $            [   "          9   5   :   =           J   Z   -   a      W      B       .   '   6   >         `         ,   Y   C   ;       Q   &   _   L          8             \       M                           
   S   G       7       ^       (          +   %       I       	               A   T      4                         *   @          N      1       V   H      R      ?       2   #       0   X      O   <                        ]   F       3   P   U   E          K   /      )   $   !       D    Add action Add to Desktop Add to Favorites Add to Panel All All _Settings Alternate application launcher for Xfce Amount of _items: Applications Background opacit_y: Browse the file system to choose a custom command. C_ommand: Categ_ory icon size: Clear Recently Used Copyright © 2013-2019 Graeme Gott Details Di_splay: Display by _default Edit Application... Edit _Profile Failed to edit profile. Failed to execute command "%s". Failed to launch menu editor. Failed to lock screen. Failed to log out. Failed to open settings manager. Failed to switch users. Favorites Icon Icon and title Include _favorites Ite_m icon size: Large Larger Log _Out Man Pages Menu Nam_e: Name None Normal Open URI Panel Button Pattern Position _search entry next to panel button Position cate_gories next to panel button Position commands next to search _entry Recently Used Remove From Favorites Remove action "%s"? Remove selected action Run %s Run in Terminal Search Actio_ns Search Action Select An Icon Select Command Show a menu to easily access installed applications Show application _descriptions Show application too_ltips Show cate_gory names Show generic application _names Show menu hie_rarchy Small Smaller Sort Alphabetically A-Z Sort Alphabetically Z-A Switch _Users Switch categories by _hovering The action will be deleted permanently. Title Unable to add launcher to desktop. Unable to add launcher to panel. Unable to edit launcher. Unable to open the following url: %s Use a single _panel row Very Large Very Small Web Search Whisker Menu Wikipedia _Appearance _Behavior _Cancel _Close _Commands _Delete _Edit Applications _Help _Icon: _Lock Screen _OK _Pattern: _Regular expression _Title: translator-credits Project-Id-Version: Whisker Menu
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-05-21 10:04+0000
Last-Translator: Zmicer Turok <zmicerturok@gmail.com>
Language-Team: Belarusian (http://www.transifex.com/gottcode/xfce4-whiskermenu-plugin/language/be/)
Language: be
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Дадаць дзеянне Дадаць на працоўны стол Дадаць ва ўпадабанае Дадаць на панэль Усе Усе _налады Альтэрнатыўнае меню запуску праграм для Xfce Колькасць _элементаў: Праграмы Празрыстасць фону Аглядзець файлавую сістэму для выбару адвольнага загаду. За_гад: Памер значка _катэгорыі: Ачысціць гісторыю Copyright © 2013-2019 Graeme Gott Падрабязнасці Ад_люстроўваць: Прадвызначана _адлюстроўваць Рэдагаваць праграму... Рэдагаваць _профіль Адрэдагаваць профіль не атрымалася. Не атрымалася запусціць загад "%s". Не атрымалася запусціць рэдактар меню. Не атрымалася заблакаваць экран. Не атрымалася выйсці. Не атрымалася адкрыць кіраўніка налад. Не атрымалася змяніць карыстальніка. Упадабанае Значок Значок і загаловак Уключаць _упадабанае Па_мер значка элемента: Буйны Вялікі Выйсці Старонкі man Меню Наз_ва: Назва Няма Звычайны Адкрыць URI Кнопка на панэлі Шаблон Радок по_шуку каля панэлі Катэгорыя пасля кнопак панэлі Кнопкі загадаў побач з радком пошуку Нядаўнія Выдаліць з упадабанага Выдаліць дзеянне "%s"? Выдаліць абранае дзеянне Запусціць %s Запусціць у тэрмінале Дзея_нні пошуку Дзеянне пошуку Абраць значок Абраць загад Адлюстроўвае меню для зручнага доступу да ўсталяваных праграм Адлюстроўваць апісанне праграм Адлюстроўваць выплыўныя апавяшчэнні для прагам Адлюстроўваць назвы катэ_горый Адлюстроўваць агульныя назвы _прагам Адлюстроўваць іерархію меню Маленькі Вельмі маленькі Упарадкаванне па алфавіце А-Я Упарадкаванне па алфавіце Я-А Змяніць _карыстальніка Пераключаць катэгорыі пры навядзенні курсора мышы Дзеянне будзе назаўсёды выдалена. Загаловак Немагчыма дадаць запускальнік на працоўны стол. Немагчыма дадаць запускальнік на панэль. Немагчыма змяніць запускальнік. Немагчыма адкрыць спасылку: %s Выкарыстоўваць адзін _радок на панэлі: Вельмі вялікі Вельмі малы Пошук у сеціве Меню Whisker Вікіпедыя _Выгляд _Паводзіны _Скасаваць _Закрыць _Загады _Выдаліць _Рэдагаваць праграмы _Даведка _Значок _Заблакаваць экран _Добра _Шаблон: _Рэгулярны выраз _Загаловак: Zmicer Turok <zmicerturok@gmail.com> 